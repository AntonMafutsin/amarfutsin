package com.beeFly.Concrete;

import com.beeFly.Abstract.IMovable;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Player extends GameCell implements  IMovable {
    int xOffset;
    int yOffset;

    @Override
    public void onStart(){
        setDisplaySymbol('P');
    }
    public void readKeys(){
        xOffset =0;
        yOffset =0;
        setCoordinates(getDirection());
    }
    private int getDirection(){
        int direction ;
        try {
            BufferedReader br = new BufferedReader(
                    new InputStreamReader(System.in));
            direction = Integer.parseInt(br.readLine());
        } catch (IOException ex){
            direction =0;
        }
        catch (NumberFormatException ex){
            direction =0;
        }
        return direction;
    }

     void setCoordinates(int direction){
        if (direction ==2){
            xOffset +=1
        ;
            yOffset +=0;
        }
        else if (direction ==8){
            xOffset += -1;
            yOffset +=0;
        }
        else if (direction ==4){
            xOffset += 0;
            yOffset +=-1;
        }
        else if (direction==6){
            xOffset += 0;
            yOffset += 1;
        }
    }

    @Override
    public int getXOffset() {
        return xOffset;
    }

    @Override
    public int getYOffset() {
        return yOffset;
    }
}