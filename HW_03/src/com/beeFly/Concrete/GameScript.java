package com.beeFly.Concrete;

import com.beeFly.Abstract.*;

public class GameScript extends Script implements ITick {

    static private GameScript defaultGameScript;

    public static  GameScript getGameScript (){
        if (defaultGameScript == null) {
            defaultGameScript = new GameScript();
        }
        return defaultGameScript;
    }

    GameField gameField;

    Player player;

    int xPlayerCoord;
    int yPlayerCoord;

    int xPrincessCoord;
    int yPrincessCoord;

    @Override
    public void onStart() {
        Time.getDefault().add(this::onTick);
        EntityStorage.getDefaultEntityStorage().add(gameField);

        xPrincessCoord = 9;
        yPrincessCoord =0;
        gameField.getField()[xPrincessCoord][yPrincessCoord] = new Princess();
        EntityStorage.getDefaultEntityStorage().add(gameField.getField()[xPrincessCoord][yPrincessCoord]);

        xPlayerCoord = 0;
        yPlayerCoord =9;
        player =  new Player();
        gameField.getField()[xPlayerCoord][yPlayerCoord] = player;
        EntityStorage.getDefaultEntityStorage().add(player);
    }

    @Override
    public void onDestroy() {
        gameField = null;
    }

    @Override
    public IRenderView setupRenderView() {
        gameField = new GameField();
        return gameField;
    }

    @Override
    public void onTick() {
            player.readKeys();
            if (!checkNewPosition(player)){
                return;
            }
            changePosition(player);
            checkWin();
    }
    private void checkWin() {
        if (yPlayerCoord == yPrincessCoord&&xPlayerCoord==xPrincessCoord) {
            gameField.viewAll();
            System.out.println("  W I N ! ! !  ");
            GameScript.getGameScript().stop();
        }
    }

    private void changePosition(Player player){
        GameCell gameCell = new GameCell();
        gameField.getField()[xPlayerCoord][yPlayerCoord] = gameCell;
        EntityStorage.getDefaultEntityStorage().add(gameCell);

        xPlayerCoord += player.getXOffset();
        yPlayerCoord +=player.getYOffset();
        gameField.getField()[xPlayerCoord][yPlayerCoord] = player;
    }

    private boolean checkNewPosition(Player player) {
        int newXCoord =player.getXOffset()+xPlayerCoord;
        int newYCoord =player.getYOffset()+yPlayerCoord;
        if (newXCoord>= GameField.WIDTH||newYCoord>=GameField.HIGHT){
            return false;
        }
        if (newXCoord<0||newYCoord<0){
            return false;
        }
        if (player.getYOffset()==0 && player.getXOffset()==0) {
            return false;
        }
        if (!gameField.getField()[newXCoord][newYCoord].isPassable()) {
            return false;
        }
        return true;
    }
}
