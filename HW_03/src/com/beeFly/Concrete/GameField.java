package com.beeFly.Concrete;

import com.beeFly.Abstract.*;
import java.util.Random;

public class GameField implements IEntity,  IRenderView {

    private GameCell[][] field;

    public static final int HIGHT = 10;
    public static final int WIDTH = 10;

    @Override
    public void onStart() {
        field = new GameCell[WIDTH][HIGHT];
        simpleInitCells();
        setObstacles();
    }

    private void setObstacles() {
        Random rnd = new Random();
        for (int i = 0; i < HIGHT ; i++) {
            field[rnd.nextInt(WIDTH)][i].setPassable(false);
        }
    }

    @Override
    public void onDestroy() {
        field = null;
    }

    private void simpleInitCells(){
        for (int i = 0; i< HIGHT; i++){
            for (int j = 0; j< WIDTH; j++){
                field[i][j] = new GameCell();
                EntityStorage.getDefaultEntityStorage().add(field[i][j]);
            }
        }
    }
    private void simpleDisplay(){
        for (int i = 0; i< HIGHT; i++) {
            for (int j = 0; j < WIDTH; j++) {
                field[i][j].display();
            }
            System.out.println();
        }
    }

    @Override
    public void viewAll() {
        simpleDisplay();
    }

    public GameCell[][] getField(){
        return field;
    }
}