package com.beeFly.Concrete;

import com.beeFly.Abstract.IDisplayable;
import com.beeFly.Abstract.IEntity;

public class GameCell implements IEntity, IDisplayable {

    private char displaySymbol;
    private boolean passable;

    public boolean isPassable(){
        return passable;
    }
    public void setPassable(boolean passable){
        this.passable = passable;
    }

    @Override
    public void display() {
        System.out.print(displaySymbol);
    }

    @Override
    public void setDisplaySymbol(char symbol) {
        displaySymbol = symbol;
    }

    @Override
    public void onStart() {
        displaySymbol = '*';
        passable = true;
    }

    @Override
    public void onDestroy() {

    }
}
