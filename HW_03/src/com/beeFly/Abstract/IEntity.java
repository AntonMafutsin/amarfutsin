package com.beeFly.Abstract;

public interface IEntity{
    void onStart();
    void onDestroy();
}
