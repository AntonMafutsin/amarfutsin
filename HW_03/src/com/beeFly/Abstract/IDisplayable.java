package com.beeFly.Abstract;

public interface IDisplayable {
    void display();
    void setDisplaySymbol(char symbol);
}
