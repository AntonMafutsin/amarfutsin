package com.beeFly.Abstract;

import java.util.ArrayList;
import java.util.List;

public class EntityStorage {
    static private  EntityStorage defaultEntityStorage;

    public static EntityStorage getDefaultEntityStorage() {
        if (defaultEntityStorage == null) {
            defaultEntityStorage = new EntityStorage();
        }
        return defaultEntityStorage;
    }
    private List<IEntity> entities;
    public  EntityStorage(){
        entities = new ArrayList<IEntity>();
    }
    public void stop(){
        for (IEntity item : entities) {
            item.onDestroy();
        }
    }
    public IEntity add(IEntity entity){
        entity.onStart();
        entities.add(entity);
        return entity;
    }
}
