package com.beeFly.Abstract;

public interface IMovable extends IDisplayable {
    int getXOffset();
    int getYOffset();
}
