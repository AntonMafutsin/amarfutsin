package com.beeFly.Abstract;

import java.util.ArrayList;
import java.util.List;

public class Time{

    static private Time defaultTime;
    public static Time getDefault() {

        return defaultTime;
    }
    public static void setDefault(Time time) {
        defaultTime = time;
    }
    private List<ITick> ticks;
    private boolean isWorking;
    private IRenderView renderView;
    public  Time(IRenderView renderView) {
        this.renderView = renderView;
        isWorking =true;
        ticks = new ArrayList<ITick>();
    }
    public void add(ITick tick){
        ticks.add(tick);
    }

    public void start(){
        while (isWorking){
            renderView.viewAll();
            for (ITick item: ticks) {
                item.onTick();
            }
        }
    }
    public  void stop(){
        isWorking = false;
    }
}
