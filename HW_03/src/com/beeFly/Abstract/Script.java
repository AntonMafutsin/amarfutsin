package com.beeFly.Abstract;

public abstract class Script implements IEntity {

    public abstract IRenderView setupRenderView();

    public void start(){
        Time.setDefault(new Time(setupRenderView()));
        onStart();
        Time.getDefault().start();
    }
    public void stop() {
        onDestroy();
        EntityStorage.getDefaultEntityStorage().stop();
        Time.getDefault().stop();
    }
}
