package com.beeFly.Tests;

import com.beeFly.Parser;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ParserTest {
    @Test
    public void testMakeIntArrFromStringArrWhenInputCorrect() {
        //arrange
        String[] correctNumbersInput = {"0","1","2","3"};

        int lengthOfArray =4;
        //act
        int[] correctNumbersInt = Parser.makeIntArrFromStringArr(correctNumbersInput,lengthOfArray);
        //assert

        for (int i = 0;i < correctNumbersInt.length;i++){
            assertEquals(correctNumbersInt[i], i);
        }
    }
    @Test
    public void testMakeIntArrFromStringArrWhenInputWrong () {
        //arrange
        String[] wrongNumbersInput1 = {"1","!@#$%^&*()","asd","ads"};
        String[] wrongNumbersInput2 = new String[0];

        int lengthOfArray =4;

        //act
        int[] wrongNumbersInt1 = Parser.makeIntArrFromStringArr(wrongNumbersInput1,lengthOfArray);
        int[] wrongNumbersInt2 = Parser.makeIntArrFromStringArr(wrongNumbersInput2,lengthOfArray);
        int[] wrongNumbersInt3 = Parser.makeIntArrFromStringArr(wrongNumbersInput2,-100);

        //assert
        assertEquals(wrongNumbersInt1,null);
        assertEquals(wrongNumbersInt2,null);
        assertEquals(wrongNumbersInt3,null);
    }
}