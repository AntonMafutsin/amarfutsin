package com.beeFly.Tests;

import com.beeFly.FormulaHolder;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class FormulaHolderTest {

    @Test
    public void getFormulaAnswerTest() {
        //Act
        double answer = FormulaHolder.getFormulaAnswer(1, 1, 5, 1);
        //Assert
        double actual = 6.579736267392906;
        assertEquals(answer, actual);
    }
}