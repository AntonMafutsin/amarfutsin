package com.beeFly;

public class Parser {

    public static int[] makeIntArrFromStringArr(String[] args, int lengthOfIntArray) {
        int[] arrNum;

        try {
            arrNum = new int[lengthOfIntArray];
        }catch (NegativeArraySizeException e) {
            System.err.println("Неверный размер массива!");
            return null;
        }

        for (int iString =0 ; iString < arrNum.length ; iString++) {
            try {
                arrNum[iString] = Integer.valueOf(args[iString]);
            }catch (NumberFormatException e) {
                System.err.println("Данные неверны в " + iString + " параметре");
                return  null;
            }catch (ArrayIndexOutOfBoundsException e){
                System.err.println("Неверный размер массива!");
                return null;
            }
        }
        return  arrNum;
    }
}
