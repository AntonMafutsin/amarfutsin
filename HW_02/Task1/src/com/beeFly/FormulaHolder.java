package com.beeFly;

public class FormulaHolder {
    public static final int NUMBER_OF_PARAMS = 4;

    public static double getFormulaAnswer(int a, int p, double m1, double m2) {
        return  ( 4* Math.pow(Math.PI,2) * ( Math.pow(a,3) )) / (Math.pow(p,2) *(m1+m2));
    }
}
