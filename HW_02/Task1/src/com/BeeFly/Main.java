package com.beeFly;

public class Main {
    public static void main(String[] args) {
        int[] params = Parser.makeIntArrFromStringArr(args , FormulaHolder.NUMBER_OF_PARAMS);

        if (params !=null) {
            System.out.print(FormulaHolder.getFormulaAnswer(params[0], params[1], params[2], params[3]));
        }
    }
}
