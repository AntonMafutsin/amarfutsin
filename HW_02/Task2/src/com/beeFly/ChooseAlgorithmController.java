package com.beeFly;

public class ChooseAlgorithmController {
    public static final int FIBONACHI = 1;
    public static final int FACTORIAL = 2;

    private int typeAlg;

    public ChooseAlgorithmController(int typeAlg){
        setAlgorithmType(typeAlg);
    }

    public void setAlgorithmType(int typeAlg) {
        this.typeAlg = typeAlg;
    }

    public int[] getAnswer( int typeLoop, int param) {
        int[] answer;
        if (typeAlg ==FIBONACHI){
            answer = getResult(new Fibonachi(), typeLoop, param);
            return answer;
        }
        else if (typeAlg == FACTORIAL){
            answer = getResult(new Factorial(), typeLoop, param);
            return answer;
        }
        else {
            System.err.println("Неверный тип алгоритма!");
            answer = new int[0];
            return  answer;
        }
    }

    private int[] getResult(LoopTypeChoosable loopType, int type, int param){
        return loopType.chooseLoopType(type,param);
    }
}
