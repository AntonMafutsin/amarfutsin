package com.beeFly.Tests;

import com.beeFly.ChooseAlgorithmController;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ChooseAlgorithmControllerTest {
    @Test
    public void testGetAnswerFactorial() {
        //Arrange
        ChooseAlgorithmController algController = new ChooseAlgorithmController( ChooseAlgorithmController.FACTORIAL );

        int factorialWhile = algController.getAnswer(1,5)[0];
        int factorialDoWhile = algController.getAnswer(2,5)[0];
        int factorialFor = algController.getAnswer(3,5)[0];

        //Assert
        int fact5 = 1*2*3*4*5;

        assertEquals(factorialWhile ,fact5 );
        assertEquals(factorialDoWhile,fact5 );
        assertEquals(factorialFor,fact5 );
    }
    @Test
    public void testGetAnswerFibonachi() {
        //Arrange
        ChooseAlgorithmController algController = new ChooseAlgorithmController( ChooseAlgorithmController.FIBONACHI );

        int fibonachiWhile = algController.getAnswer(1,6)[5];
        int fibonachiDoWhile = algController.getAnswer(2,6)[4];
        int fibonachiFor = algController.getAnswer(3,6)[3];

        //Assert
        int[] fibonachi6 ={0,1,1,2,3,5};
        assertEquals(fibonachiWhile, fibonachi6[5] );
        assertEquals(fibonachiDoWhile,fibonachi6[4] );
        assertEquals(fibonachiFor,fibonachi6[3] );
    }

}