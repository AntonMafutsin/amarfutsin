package com.beeFly;

interface LoopType{
    int WHILE_LOOP = 1;
    int DO_WHILE_LOOP = 2;
    int FOR_LOOP = 3;
}

abstract class LoopTypeChoosable {
    int[] chooseLoopType(int type, int param){
        if (type == LoopType.DO_WHILE_LOOP){
            return doWhileLoop(param);
        }
        else if (type == LoopType.WHILE_LOOP){
            return  whileLoop(param);
        }
        else if (type == LoopType.FOR_LOOP){
            return forLoop(param);
        }
        else {
            System.err.println("Неверный тип цикла!");
            return new int[0];
        }
    }
    abstract int[] whileLoop(int value);
    abstract int[] doWhileLoop(int value);
    abstract int[] forLoop(int value);
}

class Fibonachi extends LoopTypeChoosable {
    int[] initFirst2Numbers(int[] fibNumbers, int length){
        if (length>=0) {
            fibNumbers[0] = 0;
        }
        if (length>=1){
            fibNumbers[1] = 1;
        }
        return fibNumbers;
    }
    @Override
    int[] whileLoop(int value) {
        int[] fibNumbers = new int[value];
        fibNumbers= initFirst2Numbers(fibNumbers,value);
        int iterac =2;
        while (iterac<value){
            fibNumbers[iterac] = fibNumbers[iterac - 1] + fibNumbers[iterac - 2];
            iterac++;
        }
        return fibNumbers;
    }

    @Override
    int[] doWhileLoop(int value) {
        int[] fibNumbers = new int[value];
        fibNumbers = initFirst2Numbers(fibNumbers,value);
        if ( value<2){
            return fibNumbers ;
        }
        int iterac =2;
        do {
            fibNumbers[iterac] = fibNumbers[iterac - 1] + fibNumbers[iterac - 2];
            iterac++;
        }while (iterac<value);
        return fibNumbers;
    }

    @Override
    int[] forLoop(int value) {
         int[] fibNumbers = new int[value];
        fibNumbers = initFirst2Numbers(fibNumbers,value);
        for (int i = 2; i < value; ++i) {
            fibNumbers[i] = fibNumbers[i - 1] + fibNumbers[i - 2];
        }
        return fibNumbers;
    }
}
class Factorial extends LoopTypeChoosable {

    @Override
    int[] whileLoop(int value) {
        int[] answer = new int[1];
        answer[0] =1;
        int i = 1;
        while(i<=value) {
            answer[0] = answer[0] * i;
            i++;
        }
        return answer;
    }

    @Override
    int[] doWhileLoop(int value) {
        int[] answer = new int[1];
        answer[0] =1;
        int i = 1;
        do{
            answer[0] = answer[0] * i;
            i++;
        } while (i<=value) ;
        return answer;
    }

    @Override
    int[] forLoop(int value) {
        int[] answer = new int[1];
        answer[0] =1;
        for(int i = 1; i <= value; i++) {
            answer[0] = answer[0] * i;
        }
        return answer;
    }
}

public class Main {
    public static void main(String[] args) {
        int[] params = Parser.makeIntArrFromStringArr(args,3);
        if (params!=null){
            ChooseAlgorithmController algController = new ChooseAlgorithmController(params[0]);
            int[] answer = algController.getAnswer(params[1],params[2]);
            for (int number:answer) {
                System.out.println(number);
            }
        }
    }
}
